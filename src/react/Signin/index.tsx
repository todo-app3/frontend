import React from 'react';
import { Form, Input, Button, Card, message } from 'antd';
import styles, { SigninClassNames } from './style';
import withStyles from 'react-jss';
import IdentityClient from 'Clients/IdentityClient';
import { useHistory } from 'react-router-dom';

type SigninProps = {
  classes: Record<SigninClassNames, string>;
}

const Signin = ({ classes }: SigninProps): React.ReactElement => {
  const [form] = Form.useForm();
  const history = useHistory();

  return (
    <div className={classes.formWrap}>
      <Card title="User Portal" className={classes.card}>
        <Form form={form}>
          <Form.Item name="email" label="Email" rules={[{ required: true, message: 'Please input your email!', type: 'email' }]}>
            <Input
              placeholder="chad@example.com"
            />
          </Form.Item>
          <Form.Item name="password" label="Password" rules={[{ required: true, message: 'Please input a title!' }]}>
            <Input type="password" />
          </Form.Item>
          <Button
            type="primary"
            onClick={async (): Promise<void> => {
              await form.validateFields();
              const {
                email,
                password
              } = form.getFieldsValue();
              const response = await (new IdentityClient).signin(email, password);
              if (response.ok) {
                history.push('/todo');
              } else {
                message.error('Signin Failed.');
              }
            }}
          >
          Signin
          </Button>
        </Form>
      </Card>
    </div>
  );
};

export default withStyles(styles)(Signin);
