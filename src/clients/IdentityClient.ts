
declare const env: {
  TODO_API_URI: string;
};

const {
  TODO_API_URI
} = env;

export default class IdentityClient {
  static baseUrl = TODO_API_URI;

  signin(email: string, password: string): Promise<Response> {
    return this.makeRequest('/identity', {
      method: 'PUT',
      body: JSON.stringify({
        email,
        password
      })
    });
  }

  makeRequest(path: string, options?: RequestInit): Promise<Response> {
    return fetch(`${IdentityClient.baseUrl}${path}`, {
      ...options,
      credentials: 'include',
      headers: {
        ...options?.headers,
        'Content-Type': 'application/json; charset=utf-8'
      }
    });
  }
}
