import React, { useState, useEffect } from 'react';
import injectSheet from 'react-jss';
import styles, { TodoListClassNames } from './style';
import NewItemForm from './NewItemForm';
import TodoClient, { TodoItem } from 'Clients/TodoClient';
import { Spin, List, Button, message, Popconfirm } from 'antd';

type TodoListProps = {
  classes: Record<TodoListClassNames, string>;
}

const TodoList = ({ classes: unusedClasses }: TodoListProps): React.ReactElement => {
  const [items, setItems] = useState<TodoItem[]|undefined>();
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);

  const updateList = (): void => {
    (new TodoClient).getTodoItems(page).then(({ items, total }) => {
      setItems(items);
      setTotal(total);
    });
  };

  useEffect(() => {
    updateList();
  }, [page]);

  const deleteItem = (uid: string): void => {
    (new TodoClient).deleteItems([uid]).then(() => {
      updateList();
      message.success('Items Deleted!');
    });
  };

  return (
    <>
      <NewItemForm onCreate={(): void => updateList()} />
      {items ? (
        <List
          itemLayout="horizontal"
          dataSource={items}
          renderItem={(item): React.ReactElement => (
            <List.Item
              actions={[
                <Popconfirm
                  title="Are you sure?"
                  onConfirm={(): void => deleteItem(item.uid)}
                  key={0}
                >
                  <Button type="danger">Delete</Button>
                </Popconfirm>
              ]}
            >
              <List.Item.Meta
                title={item.title}
                description={item.description}
              />
            </List.Item>
          )}
          pagination={{
            onChange: (page): void => {
              setPage(page);
            },
            current: page,
            pageSize: 3,
            total
          }}
        />
      ) : <Spin />}
    </>
  );
};

export default injectSheet(styles)(TodoList);
