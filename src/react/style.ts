import { Styles } from 'react-jss';

export type AppClassNames = 'title';
export default {
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: '2rem',
  }
} as Styles<AppClassNames>;
