import React from 'react';
import {
  BrowserRouter,
  Route
} from 'react-router-dom';
import TodoList from './TodoList';
import { Layout } from 'antd';
import injectSheet from 'react-jss';
import styles, { AppClassNames } from './style';
import Signin from './Signin';

const { Header, Content } = Layout;

type AppProps = {
  classes: Record<AppClassNames, string>;
}

const App = ({ classes }: AppProps): React.ReactElement => {

  return (
    <BrowserRouter>
      <Route path="/" exact={true}>
        <Signin />
      </Route>
      <Route path="/todo" exact={true}>
        <Layout>
          <Header className={classes.title}>Todo List</Header>
          <Content>
            <TodoList />
          </Content>
        </Layout>
      </Route>
    </BrowserRouter>
  );
};

export default injectSheet(styles)(App);
