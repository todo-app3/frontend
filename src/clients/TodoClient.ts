
declare const env: {
  TODO_API_URI: string;
};

const {
  TODO_API_URI
} = env;

export interface TodoItem {
  title: string;
  description: string;
  uid: string;
}

export default class TodoClient {
  static baseUrl = TODO_API_URI;

  createTodoItem(title: string, description: string): Promise<object> {
    return this.makeRequest('/items', {
      method: 'post',
      body: JSON.stringify({
        title,
        description
      })
    });
  }

  getTodoItems(page: number): Promise<{ items: TodoItem[]; total: number }> {
    const query = new URLSearchParams({
      page: page.toString()
    });
    return this.makeRequest(`/items?${query.toString()}`);
  }

  deleteItems(uids: string[]): Promise<{ success: boolean }> {
    return this.makeRequest('/items', {
      method: 'DELETE',
      body: JSON.stringify({
        uids
      })
    });
  }

  makeRequest<T>(path: string, options?: RequestInit): Promise<T> {
    return fetch(`${TodoClient.baseUrl}${path}`, {
      ...options,
      credentials: 'include',
      headers: {
        ...options?.headers,
        'Content-Type': 'application/json; charset=utf-8'
      }
    }).then(res => res.json());
  }
}
