import React, { useState } from 'react';
import { Button, Input, Form, message } from 'antd';
import TodoClient from 'Clients/TodoClient';

type NewItemFormProps = {
  onCreate: Function;
}

const NewItemForm = ({ onCreate }: NewItemFormProps) => {
  const [showNewItem, setShowNewItem] = useState(false);
  const [form] = Form.useForm();

  return (
    <>
      {showNewItem ? (
        <Button
          onClick={() => setShowNewItem(false)}
        >
          Cancel
        </Button>
      ) : (
        <Button
          type="primary"
          onClick={() => {
            setShowNewItem(true);
          }}
        >New Item</Button>
      )}
      {showNewItem && (
        <Form form={form}>
          <Form.Item name="title" label="Title" rules={[{ required: true, message: 'Please input a title!' }]}>
            <Input
              placeholder="Buy Milk"
            />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input.TextArea rows={3} />
          </Form.Item>
          <Button
            type="primary"
            onClick={() => {
              const {
                title,
                description
              } = form.getFieldsValue();
              (new TodoClient).createTodoItem(title, description).then(() => {
                onCreate();
                message.success('Item Created!');
              });
            }}
          >
            Create
          </Button>
        </Form>
      )}
    </>
  );
};

export default NewItemForm;
