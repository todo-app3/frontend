import { Styles } from 'react-jss';

export type TodoListClassNames = 'wrap';
export default {
  wrap: {
    display: 'flex'
  }
} as Styles<TodoListClassNames>;
