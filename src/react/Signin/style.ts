import { Styles } from 'react-jss';

export type SigninClassNames = 'formWrap'|'card';
const styleMap: Styles<SigninClassNames> = {
  formWrap: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '5%',
  },
  card: {
    padding: '0 3% 2% 3%',
    boxShadow: '-1px 3px 16px 0px',
  }
};

export default styleMap;
